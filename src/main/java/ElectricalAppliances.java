public class ElectricalAppliances {

    String name;
    boolean turnedOn;
    int quantity;
    int powerWt;

    public ElectricalAppliances(String name, boolean turnedOn, int quantity, int powerWt) {
        this.name = name;
        this.turnedOn = turnedOn;
        this.quantity = quantity;
        this.powerWt = powerWt;
    }

    public ElectricalAppliances() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTurnedOn() {
        return turnedOn;
    }

    public void setTurnedOn(boolean turnedOn) {
        this.turnedOn = turnedOn;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPowerWt() {
        return powerWt;
    }

    public void setPowerWt(int powerWt) {
        this.powerWt = powerWt;
    }
    @Override
    public String toString(){
        return  ("ElectricalAppliances: " + name + ",  turnedOn: " + turnedOn + ", quantity: " + quantity
                + ",  power: " + powerWt + "Wt");
    }
}
